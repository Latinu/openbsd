#!/usr/bin/env sh
MAINFONT="NovaMono for Powerline:size=12"
ICONFONT="Font Awesome 6 Free:style=Solid:size=16"
BARHEIGHT=26
FGCOLOR="#ffffff"
BGCOLOR="[85]#292D3E"

clock() {
	TIME=`date +"%a %b %Y %T"`
	echo -e " $TIME"
}

groups() {
    # print current group and shown groups
    cur=`xprop -root _NET_CURRENT_DESKTOP | awk '{print $3}'`

    for wid in `xprop -root _NET_CLIENT_LIST | sed '/_LIST(WINDOW)/!d;s/.*# //;s/,//g'`; do
        grp=`xprop -id $wid _NET_WM_DESKTOP | awk '{print $3}'`
        shown="$shown $grp"
    done

    shown=`echo $shown | tr " " "\n" | sort -g | uniq`

    for g in `seq 1 9`; do
        if test $g == $cur; then
            groups="${groups} [$g] "
        elif echo "$shown" | grep -q $g; then
            groups="${groups} $g "
        else
            groups="${groups}"
        fi
    done

	echo -e " $groups"
}

title() {
    curwin=`xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | cut -f 2`

    if [ $curwin != "0x0" ]
    then
        curwin=`xprop -id ${curwin} _NET_WM_NAME | awk -F '"' '{print $2}' | cut -c -80`
    else
        curwin=""
    fi

    echo -e " $curwin"
}


battery() {
	STATUS=$(sysctl hw.sensors.acpiac0.indicator0 | grep -c On)

	if [ ${STATUS} -eq "1" ]; then
		BAT_STATUS=$(echo Connected)
	else
		BAT_STATUS=$(echo ${redf}Disconnected${reset})
	fi
}


monitors=$(xrandr | grep -o "^.* connected" | sed "s/ connected//" | wc -l)

while true; do
    panel_layout="%{l}$(groups)  $(title)  %{r} $(battery) | $(clock) "
    #panel_layout="%{l}$(groups) %{r} $(battery) | $(clock) "

    if [ $monitors -gt 1 ]; then
        echo "%{Sl}${panel_layout}%{Sf}${panel_layout} "
    else
        panel_layout="%{l}$(groups)  $(title)  %{r} $(battery) | $(clock) "
        #panel_layout="%{l}$(groups) %{r} $(battery) | $(clock) "
        echo "${panel_layout} "
    fi

	sleep 5
done | lemonbar-xft -d -g x"${BARHEIGHT}" -B "${BGCOLOR}" -F "${FGCOLOR}" -f "$MAINFONT" -f "$ICONFONT" | sh > /dev/null 2>&1
