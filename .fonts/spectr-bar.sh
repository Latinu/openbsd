#!/bin/sh

### Created by Amdac @ www.amdac.net
### This script was written to populate the status bar in spectrwm
### OpenBSD version

vol() {
	mute=$(sndioctl -n output.mute)
	if [ $mute = "1" ]; then
		echo "🔇 mute"
	else
		volume=$(sndioctl -n output.level | sed 's/\.// ; s/^0*//')
		[[ -n $volume ]] || volume=0
		echo "🔊 $((${volume} /10))%"
	fi
}
#mem() {
	#memused=$(top -d 1 | awk '/^Memory/ {print $3}' | sed 's/M\/.*//')
	#echo "| $memused MB"
#}


#temp() {
	#zone0="$(sysctl hw.sensors.cpu0.temp0 | sed 's/.*=// ; s/\..*/C/')"
	#echo "TEMP: [$zone0]"
#}

bat() {
	battery=$(apm -l)
	battcharge=$(apm -a)
	if [ $battcharge -eq 1 ]; then
		echo -n " ${battery}%"
	elif [ $battery -ge 90 ]; then
		echo -n " ${battery}%"
	elif [ $battery -ge 70 ]; then 
		echo -n " ${battery}%"
	elif [ $battery -ge 50 ]; then
		echo -n " ${battery}%"
	elif [ $battery -le 25 ]; then
		echo -n " ${battery}%"
	else
		echo -n "${battery}%"
	fi
}

Load() {
    LOAD=$(w | head -1 | awk -F ',' '{print $3 $4 $5}' | awk '{print $3" "$4" "$5}')
    echo -n "$LOAD"
}

dte() {
	dte=$(date +'%a %b %d | %R|')
	echo " $dte"
}

while (true); do
	echo "$(mem)+0<+@bg=0;$(temp)+0<+@bg=0;|$(bat)+0<+@bg=0;|$(vol)+0<+@bg=0;|$(dte)+0<" 
	sleep 2
done
