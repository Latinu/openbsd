#!/bin/sh

# Simple script to handle a DIY shutdown menu. When run you should see a bunch of options (shutdown, reboot etc.)
#
# Requirements:
# - rofi
# - systemd, but you can replace the commands for OpenRC or anything else
#
# Instructions:
# - Save this file as power.sh or anything
# - Give it exec priviledge, or chmod +x /path/to/power.sh
# - Run it

chosen=$(echo -e "[Cancel]\nLogout\nShutdown\nReboot" | rofi -dmenu -i -theme onedark)
# Info about some states are available here:
# https://www.freedesktop.org/software/systemd/man/systemd-sleep.conf.html#Description

if [[ $chosen = "Logout" ]]; then
	spectrwm -exit
elif [[ $chosen = "Shutdown" ]]; then
	    shutdown -p now
elif [[ $chosen = "Reboot" ]]; then
	    shutdown -r now
fi
