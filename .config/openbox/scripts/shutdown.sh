#!/bin/sh

function powermenu 
  options="Cancel\nLogout\nRestart\nShutdown"
selected=$(echo -e $options | rofi -dmenu -theme config1)
  if [ $selected = "Shutdown" ]; then
  poweroff
  elif [ $selected = "Restart" ]; then
  sudo init 6 
  elif [ $selected = "Logout" ]; then
  pkill -U $USER 
  elif [ $selected = "Cancel" ]; then
  return 
  fi
