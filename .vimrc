" Set FZF Default to Ripgrep (must install ripgrep)
let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --no-ignore-vcs'
" Disable compatibility with vi which can cause unexpected issues.
set nocompatible

" Enable type file detection. Vim will be able to try to detect the type of file is use.
filetype on

" Enable plugins and load plugin for the detected file type.
filetype plugin on

" Load an indent file for the detected file type.
filetype indent on

" Turn syntax highlighting on.
syntax on

" Add numbers to the file.
set number
"Show line numbers, relaive line numbers
set rnu
" Highlight cursor line underneath the cursor horizontally.
set cursorline

" Highlight cursor line underneath the cursor vertically.
set cursorcolumn

" Set shift width to 4 spaces.
set shiftwidth=4

" Set tab width to 4 columns.
set tabstop=4

" Use space characters instead of tabs.
set expandtab

" Do not save backup files.
set nobackup

" Do not let cursor scroll below or above N number of lines when scrolling.
set scrolloff=10

" Do not wrap lines. Allow long lines to extend as far as the line goes.
set nowrap

" While searching though a file incrementally highlight matching characters as you type.
set incsearch

" Ignore capital letters during search.
set ignorecase

" Override the ignorecase option if searching for capital letters.
" This will allow you to search specifically for capital letters.
set smartcase

" Show partial command you type in the last line of the screen.
set showcmd

" Show the mode you are on the last line.
set showmode

" Show matching words during a search.
set showmatch

" Use highlighting when doing a search.
set hlsearch

" Set the commands to save in history default number is 20.
set history=1000
set signcolumn=yes
set mat=2
" Enable auto completion menu after pressing TAB.
set wildmenu

" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx
" Set the special characters in a file
set listchars=tab:→\ ,nbsp:␣,trail:·,eol:↲,space:·
" 80, 100 column divider
let &colorcolumn="80,".join(range(100,999),",")
" Highlights beyond 100 look odd for wrapped lines, so for log type files with
" long lines, set only a single column
autocmd BufRead,BufNewFile *.{txt,log,conf,md} setlocal cc=80
" Visual mode pressing * searches for the current selection
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>

" Plugins
call plug#begin('~/.vim/plugged')

" Make your Vim/Neovim as smart as VSCode
Plug 'sheerun/vim-polyglot'
Plug 'mbbill/undotree'
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'w0rp/ale'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'honza/vim-snippets'
Plug 'itchyny/lightline.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'vwxyutarooo/nerdtree-devicons-syntax'
Plug 'preservim/nerdcommenter'
Plug 'ap/vim-css-color'
Plug 'fweep/vim-tabber'
Plug 'SirVer/ultisnips'
Plug 'ambv/black'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
Plug 'neoclide/coc.nvim',  {'branch': 'master', 'do': 'yarn install'}
Plug 'drewtempelmeyer/palenight.vim'
Plug 'plasticboy/vim-markdown'
Plug 'tmhedberg/SimpylFold'
call plug#end()



" => Status Line and Theme
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set termguicolors
set background=dark
colorscheme palenight
" Always show statusline
set laststatus=2
if has('gui_running')
  set guifont=SF Mono Regular:h22
endif
let g:lightline = { 'colorscheme': 'palenight' }

if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif

"For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
" < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
if (has("termguicolors"))
  set termguicolors
endif
let g:palenight_color_overrides = {
\    'black': { 'gui': '#000000', "cterm": "0", "cterm16": "0" },
\}
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" tagbar
nmap <F8> :TagbarToggle<CR>

"" fzf
nmap <C-p> :FZF<CR>

"" auto-pairs
let g:AutoPairsMultilineClose = 0 " doesn't delete next line brace when deleting current one

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => NERDTree
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Uncomment to autostart the NERDTree
" autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
"let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI =1 
let g:NERDTreeWinSize=20
let g:NERDTreeHighlightFoldersFullName = 1 " highlights the folder nam
" Run Python code (in normal mode, press <F5>)
nnoremap <F5> :w<CR>:!python3.10 %<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ultisnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"

" ALE
" disable lint on text change
let g:ale_lint_on_text_changed = 0
" Limit C/CPP linters
let g:ale_linters = {
\   'cpp': ['cc'],
\}
let g:ale_c_parse_compile_commands = 1

set tabline=%!tabber#TabLine()


" Normal mode remappings
nnoremap <C-q> :q!<CR>
nnoremap <C-w> :w!<CR>
nnoremap <C-t> :sp<CR>:terminal<CR>
